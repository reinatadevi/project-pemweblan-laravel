<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('product_name');
            $table->text('description');
            $table->integer('price');
            $table->integer('stocks');
            $table->unsignedBigInteger('users_id');
            $table->unsignedBigInteger('categories_id');
            $table->timestamps();
            $table->foreign('users_id')->references('id')->on('users')->change();
            $table->foreign('categories_id')->references('id')->on('categories')->change();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
