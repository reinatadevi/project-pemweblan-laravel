<?php

use App\Models\RoleUser;
use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RoleUser::create([
            'id' => 1,
            'role_name' => 'Super Admin'
        ]);
        RoleUser::create([
            'id' => 2,
            'role_name' => 'Admin'
        ]);
    }
}
