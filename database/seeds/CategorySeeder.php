<?php

use App\Models\Category;
use Illuminate\Database\Seeder;


class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'id' => 1,
            'name' => 'Baju Pria'
        ]);
        Category::create([
            'id' => 2,
            'name' => 'Baju Wanita'
        ]);
        Category::create([
            'id' => 3,
            'name' => 'Sepatu Pria'
        ]);
        Category::create([
            'id' => 4,
            'name' => 'Sepatu Wanita'
        ]);
        Category::create([
            'id' => 5,
            'name' => 'Celana Pria'
        ]);
        Category::create([
            'id' => 6,
            'name' => 'Celana Wanita'
        ]);
    }
}
