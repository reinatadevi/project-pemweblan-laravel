<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id' => 1,
            'role_id' => 1,
            'name' => 'Super Admin',
            'email' => 'super@super.com',
            'password' => Hash::make('supersuper')
        ]);
        User::create([
            'id' => 2,
            'role_id' => 2,
            'name' => 'Admin User',
            'email' => 'admin@admin.com',
            'password' => Hash::make('adminadmin')
        ]);
    }
}
