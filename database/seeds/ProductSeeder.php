<?php

use App\Models\Product;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $faker = Faker::create('id_ID');
        for ($i = 1; $i <= 20; $i++) {
            Product::create([
                'id' => $i,
                'users_id' => $faker->numberBetween(1, 2),
                'categories_id' => $faker->numberBetween(1, 6),
                'product_name' => $faker->firstname,
                'description' => $faker->sentence(6),
                'price' => $faker->numberBetween(20000, 1000000),
                'stocks' => $faker->numberBetween(2, 20)
            ]);
        }
    }
}
