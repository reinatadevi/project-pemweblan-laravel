<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoleUser extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'roleuser';
    protected $protected = ['id'];

    /**
     * Get the articles in the category.
     */
    public function users()
    {
        return $this->hasMany('App\Models\User');
    }
}
