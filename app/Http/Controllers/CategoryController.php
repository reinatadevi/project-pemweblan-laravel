<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Requests\StoreCategory;
use App\Http\Requests\UpdateCategory;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $categories = Category::all();
        return view('categories.category', ['category' => $categories]);
    }

    public function create()
    {
        //
    }

    public function store(StoreCategory $request)
    {
        $data = $request->only('name');

        Category::create($data);

        return redirect()->route('category.categories.index')->with('success', 'Category created successfully.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(UpdateCategory $request, Category $category)
    {
        $data = $request->only('name');

        $category->update($data);

        return redirect()->route('category.categories.index')->with('success', 'Category updated successfully');
    }

    public function destroy(Category $category)
    {
        if (Gate::denies('delete-category')) {
            return redirect()->back()->with('failed', "Admin Can't Delete Category");
        }
        $product = Product::where('categories_id', $category->id)->get();
        foreach ($product as $pr) {
            $pr->delete();
        }
        $category->delete();

        return redirect()->route('category.categories.index')->with('success', 'Category deleted successfully');
    }
}
