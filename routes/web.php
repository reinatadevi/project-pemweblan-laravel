<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// route product
Route::name('product.')->group(function () {
    Route::get('/product', 'ProductController@index')->name('index');
    Route::post('/product', 'ProductController@store')->name('store');
    Route::put('/product/update/{product}', 'ProductController@update')->name('update');
    Route::delete('/product/delete/{transaction}', 'ProductController@destroy')->name('delete');
    Route::resource('products', 'ProductController');
});

// route categories
Route::name('category.')->group(function () {
    Route::get('/category', 'CategoryController@index')->name('index');
    Route::post('/category', 'CategoryController@store')->name('store');
    Route::put('/category/update/{category}', 'CategoryController@update')->name('update');
    Route::delete('/category/delete/{category}', 'CategoryController@destroy')->name('delete');
    Route::resource('categories', 'CategoryController');
});

