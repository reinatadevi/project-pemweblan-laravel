@extends('layouts.template')

@section('title', 'Product Categories')

@section('content')
<section class="product">
  <div class="section-body">
    <div class="row">
      <div class="col-md-12 col-lg-12">
        <div class="card">
          <div class="card-header">
            <h1>Data Kategori Produk</h1>
          </div>
          <div class="card-body">
            @if (session('success'))
            <div class="alert alert-success ml-auto">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ session('success') }}
            </div>
            @elseif (session('failed'))
            <div class="alert alert-warning ml-auto">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ session('failed') }}
            </div>
            @endif
            <div class="table-responsive">
              <button type="button" class="btn btn-primary mb-4" data-toggle="modal" data-target="#addModalKategori">
                Tambah Kategori
              </button>
              <table id="datatables" class="table table-hover table-striped table-bordered ">
                <thead>
                  <tr>
                    <th scope="col" class="text-center">No</th>
                    <th scope="col" class="text-center">Nama Kategori</th>
                    <th scope="col" class="text-center">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($category as $index => $cat)
                  <tr>
                    <td class="text-center">{{ $index+1 }}</td>
                    <td class="text-center">{{ $cat->name }}</td>
                    <td class="text-center">
                      <div class="btn-action d-inline-flex">
                        <button class="btn btn-sm btn-success mx-2" title="edit" data-id="{{ $cat->id }}" data-name="{{ $cat->name }}" data-toggle="modal" data-target="#editModalKategori">
                          <i class="fas fa-edit"></i>
                        </button>
                        @can('delete-category')
                        <button class="btn btn-sm btn-danger mx-2" title="delete" data-id="{{ $cat->id }}" data-toggle="modal" data-target="#deleteModalKategori">
                          <i class="fas fa-trash"></i>
                        </button>
                        @endcan
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Modal Add Data -->
<div class="modal fade" id="addModalKategori" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addModalLabel">Tambah Data Kategori</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="{{ route('category.categories.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
          <div class="form-group">
            <label>Nama Kategori</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Nama Kategori">
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Tambah Kategori</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Edit Data -->
<div class="modal fade" id="editModalKategori" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Edit Kategori</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form id="edit_modal" method="POST" action="">
        {{csrf_field()}}
        {{method_field('put')}}

        <div class="modal-body">
          <input type="hidden" name="id" id="id" value="">
          <div class="form-group">
            <label>Nama Kategori</label>
            <input type="text" class="form-control" name="name" id="name" value="">
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button0" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Delete Data -->
<div class="modal fade" id="deleteModalKategori" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">Hapus Kategori</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form id="delete_modal" method="POST" action="">
        {{ csrf_field() }}
        {{ method_field('delete') }}

        <div class="modal-body">
          <input type="hidden" name="id" id="id" value="">
          <p>Apakah anda yakin akan menghapus data ini?</p>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Tidak</button>
          <button type="submit" class="btn btn-primary">Ya, hapus</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  $('#editModalKategori').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget)
    var id = button.data('id')
    var name = button.data('name')
    var modal = $(this)

    let url = `{{ route('category.categories.update', '') }}/${id}`;
    $('#edit_modal').attr('action', url);

    modal.find('.modal-body #id').val(id);
    modal.find('.modal-body #name').val(name);
  })

  $('#deleteModalKategori').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget)
    var id = button.data('id')
    var modal = $(this)

    let url = `{{ route('category.categories.destroy', '') }}/${id}`;
    $('#delete_modal').attr('action', url);
    modal.find('.modal-body #id').val(id);
  })
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#datatables').DataTable();
  });
</script>
@endsection