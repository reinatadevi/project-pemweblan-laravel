@extends('layouts.template')

@section('title')
Product | Pemweblan
@endsection

@section('content')
<section class="product">
  <div class="section-body">
    <div class="row">
      <div class="col-md-12 col-lg-12">
        <div class="card">
          <div class="card-header">
            <h1>Data Kategori Produk</h1>
          </div>
          <div class="card-body">
            @if (session('success'))
            <div class="alert alert-success ml-auto">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ session('success') }}
            </div>
            @elseif (session('failed'))
            <div class="alert alert-warning ml-auto">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ session('failed') }}
            </div>
            @endif
            <div class="table-responsive">
              <button type="button" class="btn btn-primary mb-4" data-toggle="modal" data-target="#addModal">
                Tambah Produk
              </button>
              <table id="datatables" class="table table-hover table-striped table-bordered ">
                <thead>
                  <tr>
                    <th scope="col" class="text-center">No</th>
                    <th scope="col" class="text-center">Nama Produk</th>
                    <th scope="col" class="text-center">Deskripsi</th>
                    <th scope="col" class="text-center" style="width: 100px">Harga</th>
                    <th scope="col" class="text-center">Stok Barang</th>
                    <th scope="col" class="text-center">Kategori</th>
                    <th scope="col" class="text-center" style="width: 100px">Updated At</th>
                    <th scope="col" class="text-center">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($products as $key=>$prod)
                  <tr>
                    <td class="text-center">{{$key+1}}</td>
                    <td class="text-center">{{$prod->product_name}}</td>
                    <td class="text-left">{{$prod->description}}</td>
                    <td class="text-left" style="width: 100px">Rp {{rupiah($prod->price)}}</td>
                    <td class="text-left">{{$prod->stocks}}</td>
                    <td class="text-left">{{$prod->category->name}}</td>
                    <td class="text-left" style="width: 100px">{{$prod->updated_at}}</td>
                    <td class="text-center">
                      <div class="btn-action d-inline-flex">
                        <button class="btn btn-sm btn-success mx-2" title="edit" data-id="{{$prod->id}}" data-product_name="{{$prod->product_name}}" data-description="{{$prod->description}}" data-price="{{$prod->price}}" data-stocks="{{$prod->stocks}}" data-categories_id="{{$prod->categories_id}}" data-toggle="modal" data-target="#editModal">
                          <i class="fas fa-edit"></i>
                        </button>
                        @can('delete-product')
                        <button class="btn btn-sm btn-danger mx-2" title="delete" data-id="{{$prod->id}}" data-toggle="modal" data-target="#deleteModal">
                          <i class="fas fa-trash"></i>
                        </button>
                        @endcan
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Modal Add Data -->
<div class="modal fade" id="addModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addModalLabel">Tambah Produk</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ route('product.products.store') }}" enctype="multipart/form-data">
          @csrf
          <div class="form-group">
            <label for="product_name">Nama Produk</label>
            <input type="text" class="form-control" name="product_name" id="product_name" placeholder="Nama Produk">
          </div>
          <div class="form-group">
            <label for="description">Deskripsi</label>
            <input type="text" class="form-control" name="description" id="description" placeholder="Deskripsi">
          </div>
          <div class="form-group">
            <label for="price">Harga</label>
            <input type="text" class="form-control" name="price" id="price" placeholder="Harga">
          </div>
          <div class="form-group">
            <label for="stocks">Stok Barang</label>
            <input type="text" class="form-control" name="stocks" id="stocks" placeholder="Stok">
          </div>
          <div class="form-group">
            <label for="categories_id">Kategori</label>
            <select class="form-control" name="categories_id" id="categories_id">
              @foreach($categories as $cat)
              <option value="{{$cat->id}}">{{$cat->name}}</option>
              @endforeach
            </select>
          </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="submit" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary">Tambah Data</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Edit Data -->
<div class="modal fade" id="editModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Edit Produk</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="edit_form" method="POST" action="">
          <!-- set action attribte to empty, the url from js -->
          {{csrf_field()}}
          {{method_field('put')}} <!-- cek change method -->
          <input type="hidden" name="id" id="id" value="">
          <div class="form-group">
            <label for="product_name">Nama Produk</label>
            <input type="text" class="form-control" name="product_name" id="product_name" value="">
          </div>
          <div class="form-group">
            <label for="description">Deskripsi</label>
            <input type="text" class="form-control" name="description" id="description" value="">
          </div>
          <div class="form-group">
            <label for="price">Harga</label>
            <input type="text" class="form-control" name="price" id="price" value="">
          </div>
          <div class="form-group">
            <label for="stocks">Stok Barang</label>
            <input type="text" class="form-control" name="stocks" id="stocks" value="">
          </div>
          <div class="form-group">
            <label for="categories_id">Kategori</label>
            <select class="form-control" name="categories_id" id="categories_id">
              @foreach($categories as $cat)
              <option value="{{$cat->id}}" @foreach($products as $value) @if($value->categories_id == $cat->id)
                selected
                @endif
                @endforeach>{{$cat->name}}
              </option>
              @endforeach
            </select>
          </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="submit" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary">Edit Data</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Delete Data -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">Hapus Produk</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="delete_form" method="POST" action="">
        <!-- set action attribte to empty, the url from js -->
        {{csrf_field()}}
        {{method_field('delete')}}
        <div class="modal-body">
          <input type="hidden" name="id" id="id" value="">
          <p>Apakah anda yakin akan menghapus data ini?</p>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="submit" data-dismiss="modal">Tidak</button>
          <button type="submit" class="btn btn-primary">Ya, hapus</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  $('#editModal').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget)
    var id = button.data('id')
    var product_name = button.data('product_name')
    var description = button.data('description')
    var price = button.data('price')
    var stocks = button.data('stocks')
    var categories_id = button.data('categories_id')
    var modal = $(this)

    let url = `{{ route('product.products.update', '') }}/${id}`; // generate url
    $('#edit_form').attr('action', url); // set edit_form action attribute  

    modal.find('.modal-body #id').val(id);
    modal.find('.modal-body #product_name').val(product_name);
    modal.find('.modal-body #description').val(description);
    modal.find('.modal-body #price').val(price);
    modal.find('.modal-body #stocks').val(stocks);
    modal.find('.modal-body #categories_id').val(categories_id);
  })

  $('#deleteModal').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget)
    var id = button.data('id')
    var modal = $(this)

    let url = `{{ route('product.products.destroy', '') }}/${id}`; // generate url
    $('#delete_form').attr('action', url); // set edit_form action attribute

    modal.find('.modal-body #id').val(id);
  })
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#datatables').DataTable();
  });
</script>
@endsection