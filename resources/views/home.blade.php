@extends('layouts.template')

@section('title', 'TOKOKU')

@section('content')
<div class="container">
    <!-- <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div> -->
    <div class="row justify-content-center text-center mt-5">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <img src="{{asset('assets/img/product.png')}}" class="img-fluid" width="320px">
                    <a href="{{route('product.products.index')}}" class="btn btn-primary">Go to Product</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body text-center">
                    <img src="{{asset('assets/img/category.png')}}" class="img-fluid" width="300px">
                    <a href="{{route('category.categories.index')}}" class="btn btn-primary">Go to Category</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection